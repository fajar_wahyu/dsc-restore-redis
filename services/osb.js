/**
 * Created by aankurniawan on 4/14/17.
 */
'use strict';

var SOAP = require('soap');
var Request = require('request');
var OSBConfig = {
  host: 'http://10.251.38.178',
  username: 'tcare',
  password: 'tcare12345',
  port1: '7477',
  port2: '7777'
};
var Moment = require('moment');
var requestIgnoreProxy = Request.defaults({ proxy: null });
var Resource = require('./http/resource');
var Boom = require('boom');
var xml2js = require('xml2js');

var AuthenticationHeader = {
  UserName: OSBConfig.username,
  PassWord: OSBConfig.password
};

var addAuthHeader = function (client, authHeader) {
  client.addSoapHeader({
    AuthenticationHeader: authHeader || AuthenticationHeader
  }, '', 'orac', 'http://www.oracle.com');
};

var getResource = function (host) {
  console.info('[OSB]', host);
  return Resource('[OSB]', host);
};

var parseResponse = function (response, payload, cb) {
  // payload is a serializable JSON object parsed by `request`
  // hence we don't need to parse it again
  if (response.statusCode === 200) {

    // Check for backend errors - note that errors come back with a 200 status code, if its an
    // error, it will have a property called 'Exception'.
    if (payload.Exception) {
      return cb(Boom.badRequest(Util.inspect(payload.Exception)));
    }

    // Happy Case
    return cb(null, payload);
  }

  return cb(Boom.badImplementation());
};

var __callAPI = function (request, options, host, callback) {
  getResource(host).get(request, options, function (err, response, payload) {
    if (err) {
      return callback(err);
    }
    parseResponse(response, payload, callback);
  });
};

var servicePaths = {
  ApplyEBill: '/Amdocs/CRM/Service/EBillEmailRegister',
  GetEBillDeliveryMode: '/Amdocs/CRM/Service/GetEBillDeliveryMode'
};
var getWSDLURL = function (endpoint) {
  return endpoint + '?WSDL';
};

var getResponse = function (status, data) {
  return {
    response: {
      status: status,
      data: data
    }
  };
};

var SOAPRequest = function (servicePath, responseCallback, processingCallback, opts) {
  if (!opts) {
    opts = {
      endpoint: OSBConfig.host + ':' + OSBConfig.port1 + servicePath,
      authenticate: true
    };
  }

  opts.request = requestIgnoreProxy;
  SOAP.createClient(getWSDLURL(opts.endpoint), opts, function (err, client) {
    if (err) {
      responseCallback(err);
      return;
    }
    if (!client) {
      responseCallback(Boom.badImplementation('Incorrect WSDL'));
      return;
    }

    var startTime, jsonRequest, messageRequest;
    client.on('message', function (request, exchangeId) {
      startTime = process.hrtime();
      messageRequest = request;
      var options = { ignoreAttrs: true, explicitArray: false, tagNameProcessors: [xml2js.processors.stripPrefix] };
      xml2js.parseString(request, options, (err, result) => {
        if (err) {
          return;
        }
        jsonRequest = result[Object.keys(result)[0]];
      });
    });
    client.on('soapError', function (err, exchangeId) {
      console.log('soapError', err);
    });
    client.on('response', function (soapResponse, messageResponse, exchangeId) {
      var timeDiff = process.hrtime(startTime);
      var timeTaken = Math.round((timeDiff[0] * 1e9 + timeDiff[1]) / 1e6);
      var soapBody, jsonResponse;
      var options = { ignoreAttrs: true, explicitArray: false, tagNameProcessors: [xml2js.processors.stripPrefix] };
      xml2js.parseString(soapResponse, options, (err, result) => {
        if (err) {
          return;
        }
        soapBody = result.Envelope.Body;
        jsonResponse = soapBody[Object.keys(soapBody)[0]];
      });

      var logData = {
        _id: jsonResponse ? jsonResponse.trx_id : '',
        timeTaken: timeTaken,
        channelid: jsonRequest.channel ? jsonRequest.channel : jsonRequest.ApplicationID,
        msisdn:  jsonRequest.msisdn ? jsonRequest.msisdn : jsonRequest.MSISDN,
        status: jsonResponse ? jsonResponse.error_code : '',
        method: messageResponse.request.method,
        uri: messageResponse.request.uri.href,
        soapRequest: messageRequest,
        soapResponse: soapResponse,
        exchangeId: exchangeId,
        dateTime: Moment().format('YYYY-MM-DD HH:mm:ss')
      };
    });
    if (opts.authenticate) {
      addAuthHeader(client, opts.authHeader);
    }

    processingCallback(client);
  });
};

var getDefaultPostRequestHeaders = function () {
  return {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  };
};

var getDefaultPostRequestHeaders = function () {
  return {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  };
};

var resendEBill = function (request, msisdn, period, pin, callback) {
  SOAPRequest(servicePaths.ResendEBill, callback, function (client) {
    var args = {
      BillResendGetDetailsInfo: {
        MSISDN: msisdn,
        BillDate: period,
        BillReturnType: 'R',
        ApplicationID: 'T-Care',
        trx_id: 'ResendEbill-' + new Date().getTime()
      }
    };

    if (pin) {
      args.BillResendGetDetailsInfo.PIN = pin;
    }
    client.BillResendGetDetails.BillResendGetDetailsSOAP.BillResendGetDetails(args, function (err, result) {
      if (!err) {
        if (result.ResendBillReturn || result.errorCode === '0000') {
          callback(null, getResponse(true, {
            message: 'myBilling.resendEBillSuccess'
          }));
        } else {
          callback(null, getResponse(false, {
            errorCode: result.errorCode,
            errorMessage: 'myBilling.resendEBillFailed'
          }));
        }

      } else {
        callback(getResponse(false, {
          errorCode: result.errorCode,
          errorMessage: 'myBilling.resendEBillFailed'
        }));
      }
    });
  });
};

var ActivityCodes = {
  I: 'Information',
  K: 'Complaint',
  P: 'Request',
  C: 'Case'
};

var objToQueryString = function (obj) {
  var str = [];
  for (var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
    }
  return str.join('&');
};

/**
 * This method is used to apply EBill.
 *
 * @param request
 * @param params, contains msisdn, ebillFormat, email, pin
 * @param callback
 */

var applyEBill = function (request, params, callback) {
  var ebillFormat = params.ebillFormat;
  var regMessage;

  // each payload rule specifies: BillFormat, isConfirmation, isEbillRegister, IsEmailRegister
  var payloadRules = {
    Email: ['Email', 0, 1, 1],
    SMS: ['SMS', 1, 1, 0],
    MMS: ['MMS', 1, 1, 0],
    EmailAndSms: ['Email and SMS', 1, 1, 1]
  };

  var selectedRule = payloadRules[ebillFormat];
  if (!selectedRule) {
    return callback(null, getResponse(false, {
      errorCode: 'INVALID_REQUEST'
    }));
  }
  var args = {
    BillFormat: selectedRule[0],
    isConfirmation: selectedRule[1],
    isEbillRegister: selectedRule[2],
    isEmailRegister: selectedRule[3],
    MSISDN: params.msisdn,
    ApplicationID: 'T-Care',
    trx_id: 'RegisterEbill-' + new Date().getTime()
  };

  if (ebillFormat.indexOf('Email') >= 0) {
    args['email'] = params.email;
  }

  if (params.pin) {
    args['pin'] = params.pin;
  }

  SOAPRequest(servicePaths.ApplyEBill, callback, function (client) {
    client.EBillEmailRegister.EBillEmailRegisterSOAP.regEmailAndEbill(args, function (err, result) {
      if (err) {
        return callback(err);
      }
      if (result.errorCode === '0000') {
        return callback(null, getResponse(true, result));
      }
      return callback(null, getResponse(false, {
        errorCode: result.errorCode,
        errorMessage: result.errorMessage
      }));
    });
  });
};

var getEBillDeliveryMode = function (request, msisdn, callback) {
  SOAPRequest(servicePaths.GetEBillDeliveryMode, callback, function (client) {
    var args = {
      msisdn: msisdn,
      channel: 'T-Care',
      trx_id: 'UC-GetEBillDeliveryMode-' + new Date().getTime()
    };

    client.GetEBillDeliveryMode.GetEBillDeliveryModeSOAP.GetEBillDeliveryMode(args, function (err, result) {
      if (!err) {
        if (result.error_code === '0000') {
          if (result.eBill && result.eBill.length > 0) {
            callback(null, getResponse(true, {
              eBill: result.eBill[0]
            }));
          } else {
            callback({
              errorCode: 'noEBillDeliveryMode',
              errorMessage: 'eBillDeliveryMode.notFound'
            });
          }
        } else {
          callback({
            errorCode: result.error_code,
            errorMessage: result.error_msg
          });
        }

      } else {
        callback(err);
      }
    });
  });
};

var createInteraction = function (params, callback) {
  SOAPRequest(servicePaths.CreateInteraction, callback, function (client) {
    var interactionRequest = {
      applicationID: params.applicationID,
      type: params.type,
      direction: params.direction,
      medium: params.medium,
      channel: params.channel,
      Username: params.username,
      MSISDN: params.msisdn,
      siteId: params.siteId || null,
      siteName: params.siteName || null,
      UserId: params.userId || null,
      Notes: params.notes || null
    };
    var topicRequest = {
      reasonCode1: params.reasonCode1,
      reasonCode2: params.reasonCode2,
      result: params.result,
      entityType: params.entityType
    };
    var args = {
      InteractionRequest: interactionRequest,
      TopicRequest: topicRequest,
      trx_id: params.transactionId || 'TRX-CREATE-INTERACTION-' + new Date().getTime()
    };

    client.InteractionCreate.InteractionCreateSOAP.InteractionCreate(args, function (err, result) {
      if (err) {
        return callback(err);
      }
      callback(null, result);
    });
  });
};

var updateSimcardAttributes = function (msisdn, transactionId, isAging, callback) {
  SOAPRequest(servicePaths.UpdateSimcardAttributes, callback, function (client) {
    var args = {
      trx_id: transactionId,
      msisdn: msisdn,
      activityAttributes: {
        value: isAging ? 'Y' : 'SPTP', //changed from SPTP
        name: 'MigrateInd'
      },
      channel: 'DSC' //Changed from TC
    }
    client.srm_attributes_update.srm_attributes_updateSOAP.srm_attributes_update(args, function (err, result) {
      if (err) {
        return callback(err);
      }

      if (result.error_code === '0000') {
        callback(null, {
          status: true
        });
      } else {
        callback(null, {
          status: false,
          data: {
            errorCode: result.error_code,
            errorMessage: result.error_msg
          }
        });
      }

    });
  });
};

var updateMSISDNStatus = function (msisdn, activity, callback) {
  SOAPRequest(servicePaths.ReserveRetrieveMSISDN, callback, function (client) {
    var args = {
      trx_id: 'ReserveNumber-' + new Date().getTime(),
      ApplicationID: 'DSC',
      UnifiedResourceBulkActivityListInfo: {
        UnifiedResourceActivityInfo: {
          ActivityInfo: {
            activityName: activity
          },
          RMEntityIdInfo: {
            type: 'MSISDN',
            value: msisdn
          }
        }
      }
    };
    client.NumberReserveRetrieve.NumberReserveRetrieveSOAP.reserveRelease(args, function (err, result) {
      if (err) {
        return callback(err);
      }

      if (result.errorCode === '0000') {
        callback(null, {
          status: true
        });
      } else {
        callback(null, {
          status: false,
          data: {
            errorCode: result.errorCode,
            errorMessage: result.errorMessage
          }
        });
      }

    });
  });
};


module.exports = {
  applyEBill: applyEBill,
  getEBillDeliveryMode: getEBillDeliveryMode
};
