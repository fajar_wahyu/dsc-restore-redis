var DB = require('./db-pool');

var commonHandler = function (callback) {
  return function (err, results) {
    if (err) {
      return callback(err);
    }
    callback(null, results);
  };
};

var sql = function (sql, callback, db) {
  DB.command(db, sql, commonHandler(callback));
};

var sqlWithParam = function (sql, params, callback, db) {
  DB.commandWithParams(db, sql, params, commonHandler(callback));
};

var startTransaction = function (db) {
  return DB.startTransaction(db);
};

var commandWithTransaction = function (sql, params, connection, callback) {
  DB.commandWithTransaction(sql, params, connection, callback);
};

module.exports = {
  sql: sql,
  sqlWithParam: sqlWithParam,
  startTransaction: startTransaction,
  commandWithTransaction: commandWithTransaction
};