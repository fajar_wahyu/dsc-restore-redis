var dbMngr = require('./db-manager');
var db = {
  key: 'dsc',
  config: {
    host: '127.0.0.1',
    user: 'apps',
    password: 'Telkomsel321',
    database: 'dsc_preprod',
    port: 3306,
    connectionLimit: 5,
    insecureAuth : true
  }
};

var sqlWithParam = function (sql, params, callback) {
    dbMngr.sqlWithParam(sql, params, callback, db);
  };

var getEmptyHashList = function (key, callback) {
  sqlWithParam('SELECT `trx_id`, `key`, `created_at` FROM redis_empty_hash where `key` = ?', [key], callback);
};

var getUserSite = function (username, callback) {
  sqlWithParam('SELECT user.user_id, user.username, user.fullname, site.site_id, site.site_name FROM user INNER JOIN site ' +
    'ON user.site_id = site.site_id WHERE user.username = ? LIMIT 1', [username], function (err, results) {
      if (err) return callback(err);
      callback(null, results[0]);
    });
};

var insertRestoreLog = function (data, callback) {
  const sql = 'INSERT INTO redis_restore_log (trx_id, activity, param, time_taken_param_ms, status, created_param_at) ' +
    'VALUES (?,?,?,?,?,CURRENT_TIMESTAMP)';
  sqlWithParam(sql, [data.trx_id, data.activity, data.param, data.time_taken_param_ms, data.status], callback);
}

var updateRestoreLog = function (data, callback) {
  const sql = 'UPDATE redis_restore_log SET backend_url = ?, request = ?, time_taken_request_ms = ?, ' +
    'response = ?, status = ?, created_request_at = CURRENT_TIMESTAMP ' +
    'WHERE trx_id = ? AND activity = ?';
  sqlWithParam(sql, [
    data.backend_url, data.request, data.time_taken_request_ms,
    data.response, data.status, data.trx_id, data.activity
  ], callback);
}

var insertAllRestoreLog = function (data, callback) {
  const sql = 'INSERT INTO redis_restore_log (trx_id, activity, param, time_taken_param_ms, status, ' +
    'backend_url, request, time_taken_request_ms, response, created_param_at, created_request_at) ' +
    'VALUES (?,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)';
  sqlWithParam(sql, [
    data.trx_id, data.activity, data.param, data.time_taken_param_ms, data.status,
    data.backend_url, data.request, data.time_taken_request_ms, data.response,
    data.created_param_at, data.created_request_at
  ], callback);
}

var insertActivityLog = function (data, callback) {
  const sql = 'INSERT INTO redis_activity_log (activity_log, file_name, created_at) ' +
    'VALUES (?,?,CURRENT_TIMESTAMP)';
  sqlWithParam(sql, [data.activity_log, data.file_name], callback);
}

module.exports = {
  redis: {
    getEmptyHashList: getEmptyHashList,
    insertRestoreLog: insertRestoreLog,
    updateRestoreLog: updateRestoreLog,
    insertAllRestoreLog: insertAllRestoreLog,
    insertActivityLog: insertActivityLog
  },
  user: {
    getUserSite: getUserSite
  }
}