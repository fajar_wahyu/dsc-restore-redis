var Mysql = require('mysql');

var pools = [];
var getDbPool = function (db) {
  if (pools[db.key] != undefined || pools[db.key] != null) {
    return pools[db.key];
  }

  var DBMSConfig = db.config;
  DBMSConfig.multipleStatements = true;
  pools[db.key] = Mysql.createPool(DBMSConfig);
  return pools[db.key];
}

var destroyPool = function () {
  pools.map(function (pool) {
    pool.end(function (err) {
      console.log('exit');
    });
  });
};

process.on('exit', destroyPool);

process.on('SIGINT', destroyPool);
//catch uncaught exceptions, trace, then exit normally
process.on('uncaughtException', function(e) {
  console.log('Uncaught Exception...');
  destroyPool();
  process.exit(99);
});

var command = function(db, sql, callback) {
  getDbPool(db).getConnection(function (err, connection) {
    if (err) {
      console.log('error', err);
      return callback(err);
    }
    connection.query('SET SESSION group_concat_max_len = 100000');
    connection.query(sql, function (error, results, fields) {
      connection.release();
      if (error) {
        console.log('error', error);
        return callback(error);
      }

      callback(null, results);
    });
  });
};

var commandWithParams = function (db, sql, params, callback) {
  getDbPool(db).getConnection(function (err, connection) {
    if (err) {
        console.log('error', err);
      return callback(err);
    }
    connection.query('SET SESSION group_concat_max_len = 100000');
    connection.query(sql, params, function (error, results, fields) {
      connection.release();
      if (error) {
        console.log('error', error);
        return callback(error);
      }

      callback(null, results);
    });
  });
};

var startTransaction = function (db) {
 return new Promise(function (resolve, reject) {
    getDbPool(db).getConnection(function (err, connection) {
      if (err) {
        console.log('error', err);
        return reject(err);
      }
      connection.beginTransaction(function (err) {
        if (err) {
          return reject(err);
        }

        return resolve(connection);
      });
    });
  });
};

var commandWithTransaction = function (sql, params, connection, callback) {
  connection.query(sql, params, function (error, results, fields) {
    if (error) {
        console.log('error', error);
      return callback(error);
    }

    callback(null, results);
  });
};

module.exports = {
  getDbPool: getDbPool,
  command: command,
  commandWithParams: commandWithParams,
  startTransaction: startTransaction,
  commandWithTransaction: commandWithTransaction
};