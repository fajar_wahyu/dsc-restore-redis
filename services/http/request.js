/* jslint node: true */
'use strict';
/**
 * A wrapper around the npm request package to add the following features:
 *
 * - tracing and instrumenation of requests
 * - injecting request headers as needed
 *
 * Usage:
 * var request = require('./services/request/');
 * request.init(req, res).get('http://www.google.com', function(error, response, body) {});
 *
 * See https://github.com/mikeal/request/blob/master/README.md for more advanced usage
 */
var Request = require('request'),
  SSL = {
    enforceCipher: true,
    ciphers: 'ECDHE-RSA-AES256-SHA'
  },
  _ = require('lodash');


var defaultOptions = {
    // encoding: 'utf8', // need to check if TIBCO supports utf8 encoding by default
    // default timeout of 10000 ms, override with timeout for tibco
    timeout: parseInt(20000, 10) || 10000,
    json: true,
    // gzip: true, // need to check if TIBCO supports gzip response
    headers: { // the default headers can be injected here
    }
  }, // the default values for all http requests
  currentOptions;

// Add SSL Cipher for all requests
if (SSL.enforceCipher) {
  defaultOptions.ciphers = SSL.ciphers;
}

/**
 * injects any customer headers prefixed with `X-` sent in the request object, this will help track the user-agent etc
 * @param  {[type]} options the hashtable of request options
 */
var injectCustomHeaders = function (req, options) {

  if (!req || !req.headers || typeof (req.headers) !== 'object') {
    return;
  }

  var grep = /^x-/i;

  // merge the end-user request headers onto this request's headers,
  // with preference for explicity set headers
  Object.keys(req.headers).forEach(function (name) {
    if (grep.test(name)) {
      options.headers[name] = options.headers[name] || req.headers[name];
    }
  });

  options.userAgent = req.headers['user-agent'] || req.headers['User-Agent'];
};

/**
 * initializes the npm request module with a set of default options
 * merged with any custom headers in the original request
 * @param  {object} req optional - the request being handled by the caller
 * @return {[type]} a proper reference to npm request with our defaults applied
 */
var init = function (req) {
  currentOptions = _.cloneDeep(defaultOptions);

  // TODO: We will rethink on what all can be removed from here.
  // injectCustomHeaders(req, currentOptions);

  return Request.defaults(currentOptions, requester);
};

/**
 * the custom requester that returns npm request with our defaults
 * @param  {object}   options the hashtable of request options
 * @param  {Function} cb    the callback
 * @return {[type]}           reference to npm request() with defaults applied
 */
var requester = function (uri, options, cb) {

  // can be invoked as request(string, function)
  // or request(string, options, function)
  // or request(options, function)
  if (arguments.length === 3 && typeof (uri) === 'string') {
    options.uri = uri;
  } else if (arguments.length < 3 && typeof (options) === 'function') {
    cb = options;
    options = uri;
  }

  delete options.service;
  delete options.transactionid;

  // a closure function that intercepts the callback before we call cb()
  var callback = function (err, response, body) {

    if (err) {
      // Dont log error here but rather in resource.js as response error
      return cb(err, null, null);
    }

    cb(err, response, body);
  };

  options.headers = _.merge(currentOptions.headers, options.headers);

  Request(options, callback);
};


module.exports = {
  init: init
};
