/* jslint node: true */
'use strict';

var Request = require('./request');
var Moment = require('moment');

module.exports = function (service, baseUrl) {

  var getResource = function (req, options, callback) {
    requestResource('GET', req, options, callback);
  };

  var updateResource = function (req, options, data, callback) {
    requestResource('PUT', req, options, data, callback);
  };

  var patchResource = function (req, options, data, callback) {
    requestResource('PATCH', req, options, data, callback);
  };

  var postResource = function (req, options, data, callback) {
    requestResource('POST', req, options, data, callback);
  };

  var deleteResource = function (req, options, callback) {
    requestResource('DELETE', req, options, callback);
  };

  var requestResource = function (method, req, options, data, callback) {

    // support GET|PUT|POST with different signatures
    if (callback === undefined && typeof data === 'function') {
      callback = data;
      data = null;
    }

    var url = baseUrl + options.path;
    var timeout = options.timeout;
    var ignoreProxy = options.ignoreProxy;

    if (options.qs) {
      url = url + '?' + options.qs;
    }

    options = {
      uri: url,
      method: method,
      headers: options.headers || {}
    };

    if (ignoreProxy) {
      options.proxy = null;
    }

    if (data && data !== '' ) {
      options.body = data;
    }

    if (timeout) {
      options.timeout = timeout;
    }

    options.service = service;
    options.transactionid = req.headers.transactionid;
    options['x-msisdn'] = req.headers['x-msisdn'];
    var timeStart = process.hrtime();

    Request.init(req)(options, function (error, response, body) {
      var timeDiff = process.hrtime(timeStart);
      var timeTaken = Math.round((timeDiff[0] * 1e9 + timeDiff[1]) / 1e6);
      var logData = {
        _id: options.transactionid,
        timeTaken: timeTaken,
        msisdn: options['x-msisdn'],
        status: (response && response.statusCode) || null,
        method: options.method,
        service: options.service,
        uri: options.uri,
        httpRequest: {
          href: req.url.href,
          param: data,
          query: req.query,
          path: req.path,
          host: req.headers.host,
          x_forwarded_for: req.headers['x-forwarded-for'],
          origin: req.headers.origin
        },
        httpResponse: {
          headers: response.headers,
          body: body,
          statusMessage: response.statusMessage,
          error: error
        },
        dateTime: Moment().format('YYYY-MM-DD HH:mm:ss')
      };
      console.log('responseInfo', logData);

      response = response || { statusCode: 500 };

      callback(error, response, body);
    });
  };

  return {
    get: getResource,
    put: updateResource,
    post: postResource,
    remove: deleteResource,
    patch: patchResource
  };
};
