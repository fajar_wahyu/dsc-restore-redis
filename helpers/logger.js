'use strict';

const Fs = require('fs');
const Moment = require('moment');

exports.log = function (key, data) {
  const path = __dirname + '/../logs/log-' + Moment().format('YYYYMMDD') + '.log';
  const formattedLog = Moment().format('DD-MM-YYYY HH:mm:ss.SSS') + '|' + key + '|' +
    (typeof data === 'object' ? JSON.stringify(data) : data) + '\n';
  Fs.appendFile(path, formattedLog, (err) => {
    if (err) {
      console.log('ERR', err);
      throw err;
    }
  });
};
