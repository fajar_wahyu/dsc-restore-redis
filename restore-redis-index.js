const Redis = require('./redis');
const Async = require('async');
const Fs = require('fs');
const LineByLine = require('n-readlines');
const DB = require('./services/db-mgmt');
const OSB = require('./services/osb');
const Logger = require('./helpers/logger');

const PENDING_SIMCARD_MIGRATE_INTERACTION = 'pending:simcard-migrate-interaction';
const PENDING_SIMCARD_PSB_INTERACTION = 'pending:simcard-psb-interaction';
const PENDING_SIMCARD_DOCUMENT_UPLOAD = 'pending:simcard-document-upload';
const PENDING_SIMCARD_DISCLAIMER_UPLOAD = 'pending:simcard-disclaimer-upload';
const PENDING_SIMCARD_REGISTER_EBILL = 'pending:simcard-register-ebill';

const LOG_TYPE = {
  DUMP: 'dump',
  SUCCESS: 'success',
  FAILED: 'failed'
};

// dev
const WEB_API_LOG_PATH = './web-api-log';
// production
// const WEB_API_LOG_PATH = '/apps/dsc_log_archive';

function getLogLine (path, activityLog, transactionId, cb) {
  const liner = new LineByLine(path);
  let line;
  Logger.log(activityLog, 'finding transactionId ' + transactionId + ' ...');
  while (line = liner.next()) {
    const strLine = line.toString();
    if (strLine.indexOf(activityLog) >= 0 && strLine.indexOf(transactionId) >= 0) {
      cb(strLine);
      break;
    }
  }
}

function getLog (trxId, activityLog, cb) {
  let timeStart = process.hrtime(), timeDiff, timeTaken;
  Fs.readdir(WEB_API_LOG_PATH, function (err, files) {
    if (err) {
      Logger.log('getLog', err);
    } else {
      let found = false;
      Logger.log('getLog', { trxId: trxId, activityLog: activityLog, totalFile: files.length });
      for (let i = 0; i < files.length; i++) {
        if (found) break;
        const path = WEB_API_LOG_PATH + '/' + files[i];
        getLogLine(path, activityLog, trxId, function (line) {
          found = true;
          timeDiff = process.hrtime(timeStart);
          timeTaken = Math.round((timeDiff[0] * 1e9 + timeDiff[1]) / 1e6);
          cb(null, { timeTaken: timeTaken, data: line, path: path });
        });
      }
      if (!found) {
        timeDiff = process.hrtime(timeStart);
        timeTaken = Math.round((timeDiff[0] * 1e9 + timeDiff[1]) / 1e6);
        cb({ timeTaken: timeTaken, trxId: trxId, message: 'NOT FOUND' });
      }
    }
  });
}

function getEmptyHashTransaction (key) {
  DB.redis.getEmptyHashList(key, function (err, results) {
    if (results) {
      Logger.log(key, 'total empty hash: ' + results.length);
      Async.eachOfSeries(results, function (item, sequence, nextElement) {
        Logger.log(key, 'sequence: ' + sequence + ' of ' + (results.length - 1));
        new Promise(function (resolve, reject) {
          let activity;
          switch (key) {
            case PENDING_SIMCARD_MIGRATE_INTERACTION:
              activity = 'MigrateSimcard';
              break;
            case PENDING_SIMCARD_PSB_INTERACTION:
              activity = 'PSBKartuHalo';
              break;
          }

          getLog(item.trx_id, activity, function (err, resultLog) {
            if (err) {
              reject(err);
            } else {
              const auditTrailLog = resultLog.data.split('|');
              // 0: date time
              // 1: ipAddress
              // 2: #username
              // 3: #siteName
              // 4: #msisdn
              // 5: logActionType: MigrateSimcard
              // 6: browser url
              // 7: api url
              // 8: #personalData, activationData, transactionId
              // 9: browser info
              DB.user.getUserSite(auditTrailLog[2], function (err, userSite) {
                if (err) reject(err);
                const kipParams = {
                  msisdn: auditTrailLog[4],
                  userId: userSite.username,
                  username: userSite.fullname,
                  siteId: userSite.site_id,
                  siteName: userSite.site_name,
                  transactionId: JSON.parse(auditTrailLog[8]).transactionId
                };
                Logger.log('getUserSite-kipParams', kipParams);
                const kipRestoreData = {
                  trx_id: item.trx_id,
                  activity: activity,
                  param: JSON.stringify(kipParams),
                  time_taken_param_ms: resultLog.timeTaken,
                  status: LOG_TYPE.DUMP
                }
                DB.redis.insertRestoreLog(kipRestoreData, function (err) {
                  if (err) reject(err);
                });

                // E-Bill
                OSB.getEBillDeliveryMode({}, auditTrailLog[4], function (err, ebillDelivery) {
                  Logger.log('getEBillDeliveryMode', {
                    err: err,
                    ebillDelivery: ebillDelivery,
                    msisdn: auditTrailLog[4],
                    transactionId: item.trx_id,
                    activity: activity
                  });
                  if (err) reject(err);
                  if (ebillDelivery.response.status) {
                    if (!(ebillDelivery.response.data.eBill.billFormat === 'EmailAndSMS' || ebillDelivery.response.data.eBill.billFormat === 'Email')) {
                      const ebillParams = {
                        ebillFormat: 'EmailAndSms',
                        email: JSON.parse(auditTrailLog[8]).personalData.emailAddress,
                        msisdn: auditTrailLog[4]
                      };
                      OSB.applyEBill({}, ebillParams, function (err, ebillData) {
                        Logger.log('applyEBill-ebillData', ebillData);
                        const ebillRestoreData = {
                          trx_id: item.trx_id,
                          activity: 'E-Bill',
                          param: JSON.stringify(ebillParams),
                          time_taken_param_ms: resultLog.timeTaken
                        };
                        if (err) {
                          Logger.log('applyEBill-err', err);
                          ebillRestoreData.status = LOG_TYPE.FAILED;
                          ebillRestoreData.response = JSON.stringify(err);
                          DB.redis.insertAllRestoreLog(ebillRestoreData, function (err) {
                            if (err) reject(err);
                          });
                          reject(err);
                        } else if (ebillData.response.status) {
                          Logger.log('applyEBill-success', ebillData);
                          ebillRestoreData.status = LOG_TYPE.SUCCESS;
                          ebillRestoreData.response = JSON.stringify(ebillData);
                          DB.redis.insertAllRestoreLog(ebillRestoreData, function (err) {
                            if (err) reject(err);
                          });
                          resolve(ebillData);
                        } else {
                          Logger.log('applyEBill-failed', ebillData);
                          ebillRestoreData.status = LOG_TYPE.FAILED;
                          ebillRestoreData.response = JSON.stringify(ebillData);
                          DB.redis.insertAllRestoreLog(ebillRestoreData, function (err) {
                            if (err) reject(err);
                          });
                          reject(ebillData);
                        }
                      });
                    } else {
                      reject({ msisdn: auditTrailLog[4], message: 'IGNORE Apply E-Bill', ebillDelivery: ebillDelivery });
                    }
                  } else {
                    reject({ msisdn: auditTrailLog[4], message: 'DELIVERY STATUS FALSE', ebillDelivery: ebillDelivery });
                  }
                });
              });
            }
          });
        }).then((data) => {
          Logger.log('onResolve', data);
          nextElement();
        }).catch((err) => {
          Logger.log('onReject', err);
          nextElement();
        });
      }, (err) => {
        if (err) {
          Logger.log('END OF REQUEST ERROR', {
            key: key, err: err
          });
        } else {
          Logger.log('END OF REQUEST SUCCESS', {
            key: key
          });
        }
      });
    } else {
      Logger.log('no empty hash for ' + key);
    }
  });
}

function preprocessing () {
  Fs.readdir(WEB_API_LOG_PATH, function (err, files) {
    if (err) {
      Logger.log('preprocessing', err);
    } else {
      Async.eachOfSeries(files, function (element, sequence, nextElement) {
        new Promise(function (resolve, reject) {
          const path = WEB_API_LOG_PATH + '/' + element;
          saveActivityLog(path);
          resolve();
        }).then(() => {
          Logger.log('preprocessing', 'sequence: ' + sequence + ' of ' + (files.length - 1));
          nextElement();
        }).catch(() => { nextElement(); });
      }, function (err) {
        if (err) {
          Logger.log('preprocessing end with error', { err: err });
        } else {
          Logger.log('preprocessing end successfully', { status: true });
        }
      });
    }
  });
}

function saveActivityLog (path) {
  const liner = new LineByLine(path);
  let line;
  Logger.log('saveActivityLog' + path + ' ...');
  while (line = liner.next()) {
    const strLine = line.toString();
    if (strLine.indexOf('MigrateSimcard') >= 0 || strLine.indexOf('P2P') >= 0 || strLine.indexOf('PSBKartuHalo') >= 0) {
      DB.redis.insertActivityLog({ activity_log: strLine, file_name: path }, function (err) {
        if (err) {
          Logger.log('insertActivityLog', { err: err })
        }
      });
    }
  }
}

// start exec
preprocessing();
// getEmptyHashTransaction(PENDING_SIMCARD_MIGRATE_INTERACTION);
// getEmptyHashTransaction(PENDING_SIMCARD_PSB_INTERACTION);
