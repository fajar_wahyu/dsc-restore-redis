config:
- redis.js (redis connection)
- restore-redis-index.js (audit trail log path to be read)
- services/db-mgmt.js (mysql connection)
