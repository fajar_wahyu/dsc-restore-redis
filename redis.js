  var Redis = require('redis');
  var Boom = require('boom');
  var redisConfig = {
    host: '127.0.0.1',
    port: '6379',
    auth: 'masterpass'
  };

  // Iniate & connect
  var initiateRedis = function (isSlave, returnBuffer) {
    var host = redisConfig.host;
    var client = returnBuffer? Redis.createClient(redisConfig.port, host, {return_buffers: true}) : Redis.createClient(redisConfig.port, host);
    client.auth(redisConfig.auth);
    return client;
  };

  var connectRedis = function (main, callback, noQuit, isSlave, returnBuffer) {
    var client = initiateRedis(isSlave, returnBuffer);

    client.on('error', function (err) {
      client.quit();
    });

    main(client);

    if (!noQuit) {
      client.quit();
    }
  };

  // Return functions
  var returnCallback = function (err, result, callback) {
    if (err) {
      if (err.output && err.output.statusCode) {
        return callback(err);
      }

      return callback(Boom.badImplementation(err));
    }

    return callback(null, result);
  };

  var returnIfError = function (err, callback) {
    if (err) {
      return callback(Boom.badImplementation(err));
    }
  };

  // Base functions
  var getAllKey = function (match, main, callback) {
    connectRedis(function (client) {
      client.smembers(match, function (err, rewardsArr) {
        if (err) {
          client.quit();
        }

        main(client, rewardsArr);

        client.quit();
      });
    }, callback, true);
  };

  var getAllKeyFromSlave = function (match, main, callback) {
    connectRedis(function (client) {
      client.smembers(match, function (err, rewardsArr) {
        if (err) {
          client.quit();
        }

        main(client, rewardsArr);

        client.quit();
      });
    }, callback, true, true);
  };

  var getHashKey = function (key, callback) {
    connectRedis(function (client) {
      client.hscan(key, '0', 'MATCH', '*', 'COUNT', '10', function (err, result) {
        returnCallback(err, result, callback);
      });
    }, callback, false, true);
  };

  var getKey = function (key, callback) {
    connectRedis(function (client) {
      client.get(key, function (err, result) {
        returnCallback(err, result, callback);
      });
    }, callback);
  };

  var deleteKey = function (key, callback) {
    connectRedis(function (client) {
      client.del(key, function (err, result) {
        returnCallback(err, result, callback);
      });
    }, callback);
  };

  var getField = function (key, field, callback) {
    connectRedis(function (client) {
      client.hget(key, field, function (err, result) {
        returnCallback(err, result, callback);
      });
    }, callback);
  };

  var setField = function (key, field, value, callback) {
    connectRedis(function (client) {
      client.hset(key, field, value, function (err, result) {
        returnCallback(err, result, callback);
      });
    }, callback);
  };

  var setHashKey = function (key, valueObject, callback) {
    connectRedis(function (client) {
      client.hmset(key, valueObject, function (err, result) {
        returnCallback(err, result, callback);
      });
    }, callback);
  };

  var incrementField = function (key, field, value, callback) {
    connectRedis(function (client) {
      client.HINCRBY(key, field, value, function (err, result) {
        returnCallback(err, result, callback);
      });
    }, callback);
  };

  // Custom functions
  var populateCache = function (activeFlashSale, activeSession, callback) {
    connectRedis(function (client) {
      var clientMulti = client.multi();

      // Set the active session
      clientMulti.set('session', activeSession);

      // Set the flash sale time as a key
      clientMulti.hmset('time',
        { 'startCounterTime': activeFlashSale.startCounterTime,
          'startTime': activeFlashSale.startTime,
          'endTime': activeFlashSale.endTime });

      var rewardThreshold = Config.get('/flashSale/rewardThreshold');

      activeFlashSale.products.forEach(function (reward) {

        // Calculate max queue per reward based on quantity
        var maxQueue = reward.quantity;
        if (reward.flashSalePoin > rewardThreshold.rewardValue) {
          maxQueue *= rewardThreshold.queueLengthMultiplier;
        }

        // Handle array attributes
        reward.display_type = reward.display_type ? reward.display_type.toString() : '';
        reward.catalog_type = reward.catalog_type ? reward.catalog_type.toString() : '';
        reward.location = reward.location ? reward.location.toString() : '';
        reward.provience = reward.provience ? reward.provience.toString() : '';
        reward.city = reward.city ? reward.city.toString() : '';

        // Append session, actual quantity, and max queue
        clientMulti.hmset('reward:' + activeSession + ':' + reward.keyword,
          _.extend(reward,
            { session: activeSession,
              actualQuantity: reward.quantity,
              maxQueue: maxQueue }));

        clientMulti.sadd('reward', 'reward:' + activeSession + ':' + reward.keyword);
      });

      clientMulti.exec(function (err, results) {
        returnCallback(err, results, callback);
      });
    }, callback);
  };

  var clearRunningSession = function (callback) {
    getAllKey('reward', function (client, rewardsArr) {
      var clientMulti = client.multi();

      // Delete time key
      clientMulti.del('time');

      // Delete session key
      clientMulti.del('session');

      // rewardsArr is the rewards data
      rewardsArr.forEach(function (reward) {
        clientMulti.del(reward);
        clientMulti.srem('reward', reward);
      });

      clientMulti.exec(function (errExec, results) {
        returnCallback(errExec, rewardsArr, callback);
      });
    }, callback);
  };

  var clearAndPopulateCache = function (callback) {
    var FlashSaleHelper = require('../helpers/flashSaleHelper');

    FlashSaleHelper.readJSON(function (errRead, json) {
      returnIfError(errRead, callback);

      var activeSession = FlashSaleHelper.getActiveSession(JSON.parse(json)['program']['sessions']);

      if (activeSession === null) {
        error = 'There\'s no current active sessions';
        return callback(Boom.badRequest(error));
      }

      clearRunningSession(function (err, oldKeys) {
        returnIfError(err, callback);

        populateCache(JSON.parse(json)['program']['sessions'][activeSession], activeSession, function (err, result) {
          returnCallback(err, oldKeys, callback);
        });
      });
    });
  };

  var reduceActualQuantity = function (key, callback) {
    incrementField(key, 'actualQuantity', -1, function (err, result) {
      returnCallback(err, result, callback);
    });
  };

  var increaseQuantity = function (key, callback) {
    incrementField(key, 'quantity', 1, function (err, result) {
      returnCallback(err, result, callback);
    });
  };

  var emptyMaxQueue = function (key, callback) {
    setField(key, 'maxQueue', 0, function (err, result) {
      returnCallback(err, result, callback);
    });
  };

  var getQuantity = function (key, callback) {
    getField(key, 'quantity', function (err, result) {
      returnCallback(err, result, callback);
    });
  };

  var getActualQuantity = function (key, callback) {
    getField(key, 'actualQuantity', function (err, result) {
      returnCallback(err, result, callback);
    });
  };

  var getMaxQueueLength = function (key, callback) {
    getField(key, 'maxQueue', function (err, result) {
      returnCallback(err, result, callback);
    });
  };

  var getFlashRewards = function (callback) {
    getAllKeyFromSlave('reward', function (client, rewardsArr) {
      var clientMulti = client.multi();

      // rewardsArr is the rewards data
      rewardsArr.forEach(function (reward) {
        clientMulti.hscan(reward, '0', 'MATCH', '*', 'COUNT', '100');
      });

      clientMulti.exec(function (errExec, results) {
        returnCallback(errExec, results, callback);
      });
    }, callback);
  };

  var getFlashRewardKeys = function (callback) {
    getAllKey('reward', function (client, rewardsArr) {
      return callback(null, rewardsArr);
    }, callback);
  };

  var endFlashSale = function (callback) {
    getAllKey('reward', function (client, rewardsArr) {
      var clientMulti = client.multi();

      // rewardsArr is the rewards data
      rewardsArr.forEach(function (reward) {
        clientMulti.hset(reward, 'quantity', 0);
        clientMulti.hset(reward, 'maxQueue', 0);
        clientMulti.hset(reward, 'actualQuantity', 0);
      });

      clientMulti.exec(function (errExec, results) {
        returnCallback(errExec, { success: 'Stock has been emptied' }, callback);
      });
    }, callback);
  };

  var clearFlashSale = function (callback) {
    clearRunningSession(function (err, activerewards) {
      returnCallback(err, activerewards, callback);
    });
  };

  var reduceQueueLength = function (key, message, callback) {
    getMaxQueueLength(key, function (errGet, currentQueueLength) {
      var error;

      if (!currentQueueLength) {
        error = 'Reward not found';
        return callback(Boom.badRequest(error));
      }

      if (currentQueueLength < 1) {
        error = 'The queue is full';
        return callback(Boom.badRequest(error));
      }

      incrementField(key, 'maxQueue', -1, function (err, result) {
        returnIfError(err, callback);

        if (result < 0) {
          error = 'The queue is full';
          return callback(Boom.badRequest(error));
        }

        updateUserWinning(message.msisdn, '', '', '', function () {
          return callback(null, result);
        });
      });
    });
  };

  var attemptRedeem = function (key, message, maxAttempt, callback) {
    var error;

    if (maxAttempt < 1) {
      error = 'Max attempt exceeded';
      return callback(Boom.badRequest(error));
    }

    getActualQuantity(key, function (errGet, actualQuantity) {
      if (errGet) {
        return callback(Boom.badImplementation(errGet));
      }

      if (actualQuantity < 1) {
        error = 'The actual stock is empty';
        return callback(Boom.badRequest(error));
      }

      incrementField(key, 'quantity', -1, function (errDec, resultDec) {
        returnIfError(errDec, callback);

        // Stock is currently empty
        if (resultDec < 0) {

          // Retry after a defined time
          return setTimeout(function () {
            incrementField(key, 'quantity', 1, function (errInc, resultInc) {
              return attemptRedeem(key, message, maxAttempt - 1, callback);
            });
          }, Config.get('/flashSale/redemption/timeoutInterval'));
        }

        return callback(null, resultDec);
      });
    });
  };

  var updateUserWinning = function (msisdn, winning, errorCode, notification, callback) {
    connectRedis(function (client) {
      var clientMulti = client.multi();
      clientMulti.hset('user:' + msisdn, 'winning', winning);
      clientMulti.hset('user:' + msisdn, 'errorCode', errorCode);
      clientMulti.hset('user:' + msisdn, 'notification', notification);
      clientMulti.exec(function (errExec, results) {
        returnCallback(errExec, results, callback);
      });
    }, callback);
  };

  var registerNewUser = function (msisdn, callback) {
    var newUser = {
      registered: true,
      tcash: '',
      winning: '',
      errorCode: '',
      notification: ''
    };

    setHashKey('user:' + msisdn, newUser, function (err, result) {
      returnCallback(err, result, callback);
    });
  };

  var checkRegister = function (msisdn, callback) {
    getField('user:' + msisdn, 'registered', function (err, result) {
      returnCallback(err, result, callback);
    });
  };

  var checkWinning = function (msisdn, callback) {
    getHashKey('user:' + msisdn, function (err, result) {
      returnCallback(err, result, callback);
    });
  };

  var getRunningSession = function (callback) {
    getKey('session', function (err, result) {
      returnCallback(err, result, callback);
    });
  };

  var getRunningSessionTime = function (callback) {
    getHashKey('time', function (err, result) {
      returnCallback(err, result, callback);
    });
  };

  var storeSimcardSteps = function (key, field, value, callback) {
    setField(key, field, value, function (err, result) {
      returnCallback(err, result, callback);
    });
  }

  var getSimcardSteps = function (key, field, callback, returnBuffer) {
    getField(key, field, function (err, result) {
      returnCallback(err, result, callback);
    });
  }

  var deleteSimcardSteps = function (key, callback, returnBuffer) {
    deleteKey(key, function (err, result) {
      returnCallback(err, result, callback);
    });
  }

  var setPersoHashKey = function (key, valueObject, callback) {
    connectRedis(function (client) {
      client.hmset(key, valueObject, function (err, result) {
        returnCallback(err, result, callback);
      });
    }, callback);
  };

  var storePersoStatus = function (key, value, callback){
    setPersoHashKey(key, value, function (err, result) {
      returnCallback(err, result, callback);
    });
  }

  var getRedisFields = function (key, fields, callback) {
    connectRedis(function (client) {
      client.hmget(key, fields, function (err, result){
        returnCallback(err, result, callback)
      })
    }, callback);
  }

  var getPersoStatus = function (key, field, callback, returnBuffer) {
    getField(key, field, function (err, result) {
      returnCallback(err, result, callback);
    });
  }

  var deletePersoStatus = function (key, callback, returnBuffer) {
    deleteKey(key, function (err, result) {
      returnCallback(err, result, callback);
    });
  }

  var addListElement = function (key, value, callback) {
    connectRedis(function (client) {
      client.lpush(key, value, function (err, result) {
        returnCallback(err, result, callback);
      });
    });
  }

  var addPendingTransaction = function (key, transactionId, callback) {
    addListElement (key, transactionId, function (err, result) {
      returnCallback(err, result, callback);
    });
  }

  var getList = function (key, callback, start, end) {
    start = start === undefined ? 0 : start;
    end = end === undefined? -1: end;
    connectRedis(function (client) {
      client.lrange(key, start, end, function (err, result) {
        returnCallback(err, result, callback);
      });
    });
  }

  var getPendingTransactions = function (key, callback) {
    getList (key, function (err, result) {
      returnCallback(err, result, callback);
    })
  }

  var getRange = function (key, start, end, callback) {
    getList (key, function (err, result) {
      returnCallback(err, result, callback);
    }, start, end);
  }

  var removeListElement = function (key, value, callback) {
    connectRedis(function (client) {
      client.lrem(key, 0, value, function (err, result){
        returnCallback(err, result, callback);
      });
    });
  }

  var deleteRedisKey = function (key, callback) {
    deleteKey(key, function (err, result) {
      returnCallback(err, result, callback);
    });
  }

  var addReservedMSISDN = function (key, data, callback) {
    addListElement(key, data.msisdn, function (err, result) {
      if(err) {
        returnCallback(err, result, callback);
      }
      else {
        var redisData = {
          reservedTime: new Date().getTime(),
          agentPOC: data.agentPOC
        }
        setHashKey(data.msisdn, redisData, function(err, result) {
          returnCallback(err, result, callback);
        });
      }
    });
  }

  var deleteHashElement = function (key, element, callback) {
    connectRedis(function(client) {
      client.hdel(key, element, function (err, result) {
        returnCallback(err, result, callback);
      });
    });
  };

  var getHash = function (key, callback) {
    connectRedis(function(client) {
      client.hgetall(key, function (err, result) {
        returnCallback(err, result, callback);
      })
    });
  }

  var setHash = function (key, data, callback) {
    data = Object.entries(data).reduce((accumulator, current) => (accumulator.concat(current)));
    connectRedis(function(client) {
      client.hmset(key, data, function (err, result) {
        returnCallback(err, result, callback);
      });
    });
  }

  module.exports = {
    clearAndPopulateCache: clearAndPopulateCache,
    reduceQueueLength: reduceQueueLength,
    reduceActualQuantity: reduceActualQuantity,
    increaseQuantity: increaseQuantity,
    emptyMaxQueue: emptyMaxQueue,
    getQuantity: getQuantity,
    getFlashRewards: getFlashRewards,
    getFlashRewardKeys: getFlashRewardKeys,
    endFlashSale: endFlashSale,
    clearFlashSale: clearFlashSale,
    attemptRedeem: attemptRedeem,
    updateUserWinning: updateUserWinning,
    registerNewUser: registerNewUser,
    checkRegister: checkRegister,
    checkWinning: checkWinning,
    getRunningSession: getRunningSession,
    getRunningSessionTime: getRunningSessionTime,
    storeSimcardSteps:storeSimcardSteps,
    getSimcardSteps: getSimcardSteps,
    deleteSimcardSteps: deleteSimcardSteps,
    storePersoStatus:storePersoStatus,
    getPersoStatus: getPersoStatus,
    deletePersoStatus: deletePersoStatus,
    getRedisFields: getRedisFields,
    setHashKey: setHashKey,
    setField: setField,
    addPendingTransaction: addPendingTransaction,
    getPendingTransactions: getPendingTransactions,
    removeListElement: removeListElement,
    deleteRedisKey: deleteRedisKey,
    addReservedMSISDN: addReservedMSISDN,
    deleteHashElement: deleteHashElement,
    addListElement: addListElement,
    getHash: getHash,
    setHash: setHash,
    getKey: getKey,
    getRange: getRange
  };